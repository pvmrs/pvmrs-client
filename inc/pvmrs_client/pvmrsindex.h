#ifndef VMRSINDEX_H
#define VMRSINDEX_H

#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <set>
#include <chrono>

#include <vmrs/vmrs.h>
#include <vmrs/keyset.h>
#include <vmrs/vmrssizes.h>
#include <vmrs/postinglist.h>
#include <vmrs/lookuptable.h>
#include <vmrs/qset.h>
#include <vmrs/arraylist.h>

#define ARRAY_MIN_SIZE 128 //bits

using namespace std;
using namespace vmrs;
using namespace std::chrono;

enum DicMode {
    EXTERNAL,
    MAX_SIZE,
    NO_RESTRICTION
};

class pVMRSIndex
{
public:
  pVMRSIndex(const string& ndataset_path, const string& param_path, const Keyset& nks, const int& nparam_size);

  void importDataset ();
  void buildVmrsIndex();


  VMRSSizes vs;

  string getDatasetPath() const;
  void setDatasetPath(const string &value);
  PostingList getPostingList () const;
  void setPostingList (const PostingList &pl);
  QSet getQSet () const;
  void setQSet (const QSet &qs);
  ArrayList getArrays () const;
  void setArrays (const ArrayList &array);
  pLookupTable getLookupTable () const;
  void setLookupTable (pLookupTable* lt);
  Keyset getKeyset () const;
  void setKeyset (const Keyset &ks);
  void setPairing (const string &param_file);
  Pairing* getPairing () const;
  const G1& getG () const;

  void load_external_dic(const string &path);

  size_t get_max_dic_size () const;
  void set_max_dic_size (size_t max_dic_size);

  void printFileSet() const;

  void calculateNormalizationFactor();
  void buildLookupTableEntry (const bitstring &lt_key, const uint32_t &size, const string &w, const bitstring &lt_vector, const size_t& ar_size);
  std::pair<string, string> createQsetEntry(const unsigned long& fid, const Zr& hash_wi, const bitstring& wi_k1_d, const float& cur_score);
  DocumentItem& createDocumentItemEntry(const DocumentItem& di, const string& w, const uint32_t& c, const int& doc_size);
  std::pair<string,string> createArrayEntry(const DocumentItem& new_di, const uint32_t& c, const int& doc_size, const bitstring& wi_k3_lambda );
  void createDummyArrayEntries(Array& cur_array, const int& doc_size);
  void createDummyArrayListEntries();
  void createDummyLookupTableEntries();
  void setSizes();


  void exportDataset(const string& path);
  void exportInfo(const string& path);

  bool is_verbose () const;
  void set_verbose (bool verbose);
  bool set_export(const string& info, const string& qset, const string& al, const string& flt, const string& dataset);
 private:
  PostingList pl;
  QSet qs;
  ArrayList array;
  pLookupTable* lt;
  unordered_map<unsigned long, float> norm_factor_set;
  vector<string> file_set;
  DatasetInfo dataset_info;
  Keyset ks;

  unordered_map<unsigned long, unordered_map<string, unsigned long>> norm_factor_list;

  string dataset_path;
  Pairing *pairing{};
  G1 g;

  bool verbose;
  high_resolution_clock::time_point start;

  size_t max_dic_size;
  DicMode dic_mode;
  void logmsg(const string& msg);

  std::ofstream f_info, f_qset, f_array, f_lt, f_ds;


};

#endif // VMRSINDEX_H
