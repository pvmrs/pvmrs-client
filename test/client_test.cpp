#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
//#define private public
#include <catch.hpp>
#include "../inc/pvmrs_client/pvmrsindex.h"

G1 getQsetKey(const ulong& fid, const string& w, const size_t& param, const size_t& zr, Pairing * p, const G1& g, const Keyset& ks)
{
  //FI k3 (wi)
  bitstring bs_w_k1 = hmac (ks.getPbcK3 (), w, zr);
  Zr zr_hash3w2 = from_bs(p, bs_w_k1);

  //xid = FI k1  (Fic)
  bitstring bs_hash1fid = hmac (ks.getPbcK1 (), to_string (fid), zr);
  Zr zr_hash1fid = from_bs (p, bs_hash1fid);

  return G1(g ^ (zr_hash1fid * zr_hash3w2));
}

TEST_CASE( "pvmrsindex") {
  string keyset_file = "input/keyset.bin";
  string pairing_file = "input/a.param";
  string dataset_path = "input/dataset";

  Keyset ks = Keyset::importKeys (keyset_file, 256, 256);

  pVMRSIndex vmrs_ind(dataset_path, pairing_file, ks, 256);

  SECTION("qset") {
    //qset fid1|qwe
    string w = "qwe";
    float score = 1.0;
    unsigned long fid = 1;


    //qset key from vmrsindex
    bitstring bs_w_k3 = hmac (ks.getPbcK3 (), w, vmrs_ind.vs.zr_size);
    Zr zr_w_k3 = from_bs (vmrs_ind.getPairing (), bs_w_k3);

    bitstring wi_k1_d = hmac (ks.getK1 (), w, vmrs_ind.vs.pi_size);

    std::pair<string, string> qset = vmrs_ind.createQsetEntry (fid, zr_w_k3, wi_k1_d, score );

    G1 qset_key(*(vmrs_ind.getPairing ()), (const unsigned char *)qset.first.data(), bit_to_byte (vmrs_ind.vs.zp_size), false, 0);
    qset_key.dump(stdout, "qset from string: ", 16);

    //qset key from test
    G1 test_key = getQsetKey(fid, w, vmrs_ind.vs.param_size, vmrs_ind.vs.zr_size, vmrs_ind.getPairing () ,vmrs_ind.getG (), ks);


      REQUIRE(qset_key == test_key);
      REQUIRE(qset.first == test_key.toString (false));


    /*
    ulong f1 = 1;
    string w1 = "asd";
    string w2 = "qwe";



    //w1
    bitstring wi_k1_d = hmac (ks.getK1 (), w1, vmrs_ind.vs.pi_size);
    bitstring wi_pbck3 = hmac (ks.getPbcK3 (), w1, vmrs_ind.vs.param_size);
    Zr hash_wi = from_bs (vmrs_ind.getPairing (), wi_pbck3);

      //bitstring bs_hash3w2 = hmac (ks.getPbcK1 (), w2, vs.param_size);
      //Zr zr_hash3w2 = from_bs(p, bs_hash3w2);

      //G1 qset_w2f1 (g ^ (zr_hash3w2 * zr_hash1fid));

    std::pair<string, string> qw1_f1 = vmrs_ind.createQsetEntry(f1, hash_wi, wi_k1_d, 1.0);
*/


  }
}



